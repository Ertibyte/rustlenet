use super::Unit;
use crate::{Float, Tensor};

pub struct Sigmoid {
    input: Option<Tensor>,
}

impl Sigmoid {
    pub fn new() -> Self {
        Self { input: None }
    }
}

impl Unit for Sigmoid {
    fn forward(&mut self, input: Tensor) -> Tensor {
        let output = input.mapv(sigmoid);
        self.input = Some(input);
        output
    }

    fn backward(&mut self, output_gradient: Tensor, learning_rate: Float) -> Tensor {
        let input = self.input.as_ref().unwrap();
        output_gradient * input.mapv(sigmoid_deriv)
    }
}

fn sigmoid(x: Float) -> Float {
    1. / (1. + (-x).exp())
}

fn sigmoid_deriv(x: Float) -> Float {
    sigmoid(x) * (1.0 - sigmoid(x))
}
