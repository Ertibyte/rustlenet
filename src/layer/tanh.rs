use super::Unit;
use crate::{Float, Tensor};

pub(crate) struct Tanh {
    input: Option<Tensor>,
}

impl Tanh {
    pub(crate) fn new() -> Self {
        Self { input: None }
    }
}

impl Unit for Tanh {
    fn forward(&mut self, input: Tensor) -> Tensor {
        let output = input.view().mapv(Float::tanh);
        self.input = Some(input);
        output
    }

    fn backward(&mut self, output_gradient: Tensor, learning_rate: Float) -> Tensor {
        let input = self.input.as_ref().unwrap();
        output_gradient * input.mapv(tanh_prime)
    }
}

fn tanh_prime(x: Float) -> Float {
    1.0 - x.tanh().powi(2)
}

#[cfg(test)]
mod tests {
    use ndarray::IxDyn;

    use super::*;
    #[test]
    fn test_tanh_layer() {
        let input =
            Tensor::from_shape_vec(IxDyn(&[2, 3]), vec![0.2, 0.1, 0.4, 0.5, 0.3, 0.6]).unwrap();
        let mut layer = Tanh::new();
        layer.forward(input);
    }
}
