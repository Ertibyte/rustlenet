use super::Unit;
use crate::{Float, Tensor};

pub struct ReLU {
    input: Option<Tensor>,
}

impl ReLU {
    pub fn new() -> Self {
        Self { input: None }
    }
}

impl Unit for ReLU {
    fn forward(&mut self, input: Tensor) -> Tensor {
        let output = input.mapv(relu);
        self.input = Some(input);
        output
    }

    fn backward(&mut self, output_gradient: Tensor, learning_rate: Float) -> Tensor {
        let input = self.input.as_ref().unwrap().mapv(relu_prime);
        output_gradient * input
    }
}

fn relu(x: Float) -> Float {
    x.max(0.0)
}

fn relu_prime(x: Float) -> Float {
    if x > 0.0 {
        1.0
    } else {
        0.0
    }
}
