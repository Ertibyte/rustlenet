use ndarray::IxDyn;
use rand::{distributions::Uniform, Rng};

use super::Unit;
use crate::{utility::math::dot, Float, Tensor};

/// Each input neuron is connected with each output neuron
pub struct Dense {
    weights: Tensor,
    bias: Tensor,
    input: Option<Tensor>,
}

impl Dense {
    /// input_size: Input neuron count
    /// output_size: Output neuron count
    pub fn new(input_size: usize, output_size: usize) -> Self {
        let mut rng = rand::thread_rng();
        let weights = Tensor::from_shape_simple_fn(IxDyn(&[input_size, output_size]), || {
            rng.sample(Uniform::new(-1.0, 1.0))
        });
        let bias = Tensor::from_shape_simple_fn(IxDyn(&[1, output_size]), || {
            rng.sample(Uniform::new(-1.0, 1.0))
        });

        Self {
            weights,
            bias,
            input: None,
        }
    }
}

impl Unit for Dense {
    fn forward(&mut self, input: Tensor) -> Tensor {
        let output = dot(&input, &self.weights) + &self.bias;
        self.input = Some(input);
        output
    }

    fn backward(&mut self, output_gradient: Tensor, learning_rate: Float) -> Tensor {
        let input = self.input.as_ref().unwrap();
        let weights_gradient = dot(&input.t().to_owned(), &output_gradient);
        let biases_gradient = output_gradient.sum_axis(ndarray::Axis(0));
        //
        self.weights -= &(learning_rate * weights_gradient);
        self.bias -= &(learning_rate * biases_gradient);
        dot(&output_gradient, &self.weights.t().to_owned())
    }
}
