use ndarray::{Array, Axis};

use crate::{utility::math::dot, Float, Tensor};

use super::Unit;

/// Softmax Activation Layer
pub struct Softmax {
    output: Option<Tensor>,
}
impl Softmax {
    pub fn new() -> Self {
        Self { output: None }
    }
}

impl Unit for Softmax {
    fn forward(&mut self, input: Tensor) -> Tensor {
        // Subtract the maximum value in each row from each element in that row
        let exp = input.mapv(Float::exp);
        let sum = exp.sum();
        let output = exp / sum;
        self.output = Some(output.clone());
        output
    }

    fn backward(&mut self, output_gradient: Tensor, _learning_rate: Float) -> Tensor {
        let output = self.output.as_ref().unwrap();
        let n = output.shape()[1];
        let identity = Array::eye(n);
        let diff = (identity - output.t());
        dot(&output_gradient, &(diff * output))
    }
}
