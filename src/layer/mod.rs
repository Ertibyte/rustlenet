mod dense;
mod relu;
mod sigmoid;
mod softmax;
mod tanh;

use crate::{Float, Tensor};

pub enum Layer {
    Dense(usize, usize),
    Tanh,
    Sigmoid,
    ReLU,
    Softmax,
}

impl Layer {
    pub fn to_unit(self) -> Box<dyn Unit> {
        match self {
            Layer::Dense(in_n, out_n) => Box::new(dense::Dense::new(in_n, out_n)),
            Layer::Tanh => Box::new(tanh::Tanh::new()),
            Layer::Sigmoid => Box::new(sigmoid::Sigmoid::new()),
            Layer::ReLU => Box::new(relu::ReLU::new()),
            Layer::Softmax => Box::new(softmax::Softmax::new()),
        }
    }
}

pub trait Unit {
    fn forward(&mut self, input: Tensor) -> Tensor;
    fn backward(&mut self, output_gradient: Tensor, learning_rate: Float) -> Tensor;
}
