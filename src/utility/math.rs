use crate::Tensor;

pub(crate) fn dot(a: &Tensor, b: &Tensor) -> Tensor {
    let shape_a = a.dim();
    let shape_b = b.dim();
    let shaped_a = a.to_shape((shape_a[0], shape_a[1])).unwrap();
    let shaped_b = b.to_shape((shape_b[0], shape_b[1])).unwrap();
    shaped_a.dot(&shaped_b).into_dyn()
}
