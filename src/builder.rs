use crate::{layer::Layer, NeuralNetwork};

pub struct NeuralNetworkBuilder {
    layers: Vec<Layer>,
    verbose: Option<usize>,
}

impl NeuralNetworkBuilder {
    pub fn new() -> Self {
        NeuralNetworkBuilder {
            layers: Vec::new(),
            verbose: None,
        }
    }

    pub fn add_layer(mut self, layer: Layer) -> Self {
        self.layers.push(layer);
        self
    }

    pub fn set_verbose(mut self, verbose: usize) -> Self {
        assert_ne!(
            verbose, 0,
            "Make sure that verbose isn't 0 to avoid division by 0"
        );
        self.verbose = Some(verbose);
        self
    }

    pub fn build(self) -> NeuralNetwork {
        let layers = self
            .layers
            .into_iter()
            .map(|layer| layer.to_unit())
            .collect();

        NeuralNetwork {
            layers,
            verbose: self.verbose,
        }
    }
}
