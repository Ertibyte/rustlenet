#![allow(unused)]
mod builder;
mod layer;
mod loss;
pub mod prelude;
mod utility;

use builder::NeuralNetworkBuilder;
use layer::Unit;
use loss::Loss;
use ndarray::{Array1, Array2, ArrayD, Axis, Zip};

pub(crate) type Float = f32;
pub(crate) type Tensor = ArrayD<Float>;

pub struct NeuralNetwork {
    layers: Vec<Box<dyn Unit>>,
    verbose: Option<usize>,
}

impl NeuralNetwork {
    /// Returns a NeuralNetworkBuilder.
    pub fn new() -> NeuralNetworkBuilder {
        NeuralNetworkBuilder::new()
    }

    pub fn train(
        &mut self,
        train_data: Tensor,
        train_labels: Tensor,
        loss: Loss,
        epochs: usize,
        learning_rate: Float,
    ) {
        let batch_size = *train_data
            .shape()
            .get(0)
            .expect("Stored the number of data rows") as Float;
        dbg!(&batch_size);

        let (loss, loss_prime): (
            fn(&Tensor, &Tensor) -> Float,
            fn(&Tensor, &Tensor) -> Tensor,
        ) = match loss {
            Loss::MSE => (loss::mse, loss::mse_prime),
            Loss::BinaryCrossEntropy => {
                (loss::binary_cross_entropy, loss::binary_cross_entropy_prime)
            }
        };
        for e in 0..epochs {
            let mut cost = 0.0;

            let x_iter = train_data.axis_iter(Axis(0));
            let y_iter = train_labels.axis_iter(Axis(0));

            Zip::from(x_iter).and(y_iter).for_each(|x, y| {
                let y_true = y.insert_axis(Axis(0)).to_owned();
                let input = x.insert_axis(Axis(0)).to_owned();
                let output = self.predict(input);
                //
                if self.verbose.is_some() {
                    cost += (loss)(&y_true, &output);
                }
                //
                let mut gradiant = (loss_prime)(&y_true, &output);

                for layer in self.layers.iter_mut().rev() {
                    gradiant = layer.backward(gradiant, learning_rate);
                }
            });
            if let Some(v) = self.verbose {
                cost /= batch_size;
                if (e + 1) % v == 0 {
                    println!("{}/{}, error={}", e + 1, epochs, cost);
                }
            }
        }
    }

    pub fn predict(&mut self, x: Tensor) -> Tensor {
        let mut output = x;
        for layer in &mut self.layers {
            output = layer.forward(output);
        }
        output
    }
}

pub fn one_hot_encode(labels: Array1<usize>, num_classes: usize) -> Tensor {
    let num_labels = labels.len();
    let mut one_hot = Array2::zeros((num_labels, num_classes));
    for (i, label) in labels.iter().enumerate() {
        let class_index = *label as usize;
        assert!(
            class_index < num_classes,
            "Label value exceeds number of classes"
        );
        one_hot[(i, class_index)] = 1.0;
    }

    one_hot.into_dyn()
}
