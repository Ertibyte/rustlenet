use crate::{Float, Tensor};

pub enum Loss {
    MSE,
    BinaryCrossEntropy,
}

pub fn mse(y_true: &Tensor, y_pred: &Tensor) -> Float {
    let diff = y_true - y_pred;
    (&diff * &diff).mean().unwrap()
}

pub fn mse_prime(y_true: &Tensor, y_pred: &Tensor) -> Tensor {
    let n = y_true.len() as Float;
    (2.0 / n) * (y_pred - y_true)
}

pub fn binary_cross_entropy(y_true: &Tensor, y_pred: &Tensor) -> Float {
    todo!()
}

pub fn binary_cross_entropy_prime(y_true: &Tensor, y_pred: &Tensor) -> Tensor {
    todo!()
}
