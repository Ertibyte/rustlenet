use mnist::{Mnist, MnistBuilder};
use ndarray::*;
use rustlenet::{one_hot_encode, prelude::*};
use std::time::Instant;
fn main() {
    // -------- -------- -------- -------- Setup
    let training_length: usize = 1000;
    let validation_length: usize = 100;
    // Deconstruct the returned Mnist struct.
    let Mnist {
        trn_img,
        trn_lbl,
        tst_img,
        tst_lbl,
        ..
    } = MnistBuilder::new()
        .label_format_digit()
        .training_set_length(training_length as u32)
        .validation_set_length(validation_length as u32)
        .test_set_length(validation_length as u32)
        .finalize();

    let train_data = Array2::from_shape_vec((training_length, 784), trn_img)
        .expect("Error converting images to Array3 struct")
        .map(|x| *x as f32 / 255.0);
    let train_labels: Array1<usize> = Array::from_vec(trn_lbl).mapv(|x| x as usize);

    let test_data = Array2::from_shape_vec((validation_length, 784), tst_img)
        .expect("Error converting images to Array3 struct")
        .mapv(|x| x as f32 / 255.);

    let test_labels: Array1<usize> = Array::from_vec(tst_lbl).mapv(|x| x as usize);

    let mut neural_network = NeuralNetwork::new()
        .add_layer(Layer::Dense(28 * 28, 10))
        .add_layer(Layer::Sigmoid)
        .add_layer(Layer::Dense(10, 10))
        .add_layer(Layer::Softmax)
        .set_verbose(25)
        .build();

    // -------- -------- Training
    let epochs = 1000;
    let learning_rate = 0.01;
    println!("Started Training");
    let bench = Instant::now();
    let y_true = one_hot_encode(train_labels, 10);
    neural_network.train(
        train_data.into_dyn(),
        y_true,
        Loss::MSE,
        epochs,
        learning_rate,
    );
    let time = bench.elapsed();
    println!("Ended Training, Training took: {:#?}", time);
    // -------- -------- -------- -------- Predict
    let y_true = one_hot_encode(test_labels, 10);
    let x_iter = test_data.axis_iter(Axis(0));
    let y_iter = y_true.axis_iter(Axis(0));

    let mut correct = 0;

    Zip::from(x_iter).and(y_iter).for_each(|x, y| {
        let y = y.insert_axis(Axis(0)).to_owned();
        let x = x.insert_axis(Axis(0)).to_owned();
        let output = neural_network.predict(x.into_dyn().to_owned());
        let (idmax, _) =
            output
                .iter()
                .enumerate()
                .fold((0, 0.0), |(id_a, val_a), (id_b, &val_b)| {
                    if val_a < val_b {
                        (id_b, val_b)
                    } else {
                        (id_a, val_a)
                    }
                });
        let (idtrue, _) = y
            .iter()
            .enumerate()
            .fold((0, 0.0), |(id_a, val_a), (id_b, &val_b)| {
                if val_a < val_b {
                    (id_b, val_b)
                } else {
                    (id_a, val_a)
                }
            });

        if idmax == idtrue {
            correct += 1;
        }
    });

    println!(
        "Predicted: {}/{}, With accuracy of: {:#.2}",
        correct,
        validation_length,
        correct as f32 / validation_length as f32
    );
}
