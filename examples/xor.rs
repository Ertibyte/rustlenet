use ndarray::{Array, Axis};
use rustlenet::prelude::*;

fn main() {
    let x = Array::from_shape_vec((4, 2), vec![0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0]).unwrap();
    let y = Array::from_shape_vec((4, 1), vec![0.0, 1.0, 1.0, 0.0]).unwrap();

    let epochs = 10_000;
    let learning_rate = 0.01;

    let verbose = 1000;

    let mut neural_network: NeuralNetwork = NeuralNetwork::new()
        .add_layer(Layer::Dense(2, 3))
        .add_layer(Layer::ReLU)
        .add_layer(Layer::Dense(3, 1))
        .add_layer(Layer::Tanh)
        .set_verbose(verbose)
        .build();

    // Train
    dbg!(&x);
    neural_network.train(
        x.clone().into_dyn(),
        y.into_dyn(),
        Loss::MSE,
        epochs,
        learning_rate,
    );
    // Prediction
    for x in x.axis_iter(Axis(0)) {
        let x = x.insert_axis(Axis(0)).to_owned();
        let z = neural_network.predict(x.clone().into_dyn());
        println!("---- ---- ----\n{:?}\n{:?}\n", x, z);
    }
}
